import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StartTest {
    @Test
    public void factorialTest() {
        Start factorial = new Start();
        int n = 0;
        int expectedValue = 1;

        long result = factorial.factorial(n);

        Assertions.assertEquals(expectedValue, result);
    }
}
