import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class BasicSelenium {
    WebDriver driver;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\domin\\IdeaProjects\\TS1\\End_To_End\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void flightFormTest() {
        String firstName = "Honza";
        String lastName = "Novak";

        // open
        driver.get("https://flight-order.herokuapp.com");
        driver.manage().window().fullscreen();

        // fill firstname
        driver.findElement(By.id("flight_form_firstName")).sendKeys(firstName);
        // fill surname
        driver.findElement(By.id("flight_form_lastName")).sendKeys(lastName);
        // fill email
        driver.findElement(By.id("flight_form_email")).sendKeys("honza.n@email.cz");
        // fill birthday
        driver.findElement(By.id("flight_form_birthDate")).sendKeys("12.7.2002");
        // select destination London
        Select selectDestination = new Select(driver.findElement(By.id("flight_form_destination")));
        selectDestination.selectByVisibleText("London");
        // select discount Senior
        driver.findElement(By.id("flight_form_discount_1")).click();

        // send form
        driver.findElement(By.id("flight_form_submit")).click();

        // check outputs
        WebElement fullname = driver.findElement(By.cssSelector("tr:nth-child(1) td"));

        Assertions.assertEquals(firstName + " " + lastName, fullname.getAttribute("textContent"));
    }

    @Test
    public void seleniumPageFactoryBasic() {
        FlightFormPage flightFormPage = new FlightFormPage(driver);
        String firstName = "Honza";
        String lastName = "Novak";
        String email = "honza.n@email.cz";
        String birthday = "12.7.2002";

        flightFormPage.setFirstName(firstName);
        flightFormPage.setLastName(lastName);
        flightFormPage.setEmail(email);
        flightFormPage.setBirthday(birthday);
        flightFormPage.sendForm();
    }
}
