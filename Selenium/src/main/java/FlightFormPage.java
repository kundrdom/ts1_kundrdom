import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.awt.print.PageFormat;

public class FlightFormPage {
    WebDriver driver;
    @FindBy(id = "flight_form_firstName")
    WebElement firstName;
    @FindBy(id = "flight_form_lastName")
    WebElement lastName;
    @FindBy(id = "flight_form_email")
    WebElement email;
    @FindBy(id = "flight_form_birthDate")
    WebElement birthday;
    @FindBy(id = "flight_form_submit")
    WebElement sendButton;

    public FlightFormPage(WebDriver driver) {
        this.driver = driver;
        driver.get("https://flight-order.herokuapp.com");
        driver.manage().window().fullscreen();
        PageFactory.initElements(driver, this);
    }

    public void setFirstName(String firstNameStr) {
        firstName.sendKeys(firstNameStr);
    }

    public void setLastName(String lastNameStr) {
        lastName.sendKeys(lastNameStr);
    }

    public void setEmail(String emailStr) {
        email.sendKeys(emailStr);
    }

    public void setBirthday(String birthdayStr) {
        birthday.sendKeys(birthdayStr);
    }

    public void sendForm(){
        sendButton.click();
    }
}
