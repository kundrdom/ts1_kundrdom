package archive;

import org.junit.jupiter.api.*;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.mockito.Mockito.*;

public class PurchasesArchiveTest {
    ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    PrintStream originalOut = System.out;
    PrintStream originalErr = System.err;
    Item item1 = mock(Item.class);
    Item item2 = mock(Item.class);
    ItemPurchaseArchiveEntry purchaseArchiveEntry1 = mock(ItemPurchaseArchiveEntry.class);
    ItemPurchaseArchiveEntry purchaseArchiveEntry2 = mock(ItemPurchaseArchiveEntry.class);
    ArrayList<Order> orderArchive = mock(ArrayList.class);
    HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchiveEntryHashMap = new HashMap();

    @BeforeEach
    public void setUpEverything() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

        when(purchaseArchiveEntry1.toString()).thenReturn("ITEM Misa   HAS BEEN SOLD 2 TIMES");
        when(purchaseArchiveEntry2.toString()).thenReturn("ITEM Mirza   HAS BEEN SOLD 5 TIMES");

        when(item1.getID()).thenReturn(1);
        when(item2.getID()).thenReturn(2);
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }


    @Test
    public void printItemPurchaseStatistics_PrintsItemStatistics_Stream() {
        itemPurchaseArchiveEntryHashMap.put(1, purchaseArchiveEntry1);
        itemPurchaseArchiveEntryHashMap.put(2, purchaseArchiveEntry2);

        String expected = "ITEM PURCHASE STATISTICS:" + "\r\n" + "ITEM Misa   HAS BEEN SOLD 2 TIMES\r\n"
                + "ITEM Mirza   HAS BEEN SOLD 5 TIMES\r\n";

        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchiveEntryHashMap, orderArchive);
        purchasesArchive.printItemPurchaseStatistics();

        Assertions.assertEquals(expected, outContent.toString());
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_TestSoldCountOfItem_TwoAndFour() {
        when(purchaseArchiveEntry1.getCountHowManyTimesHasBeenSold()).thenReturn(2);
        when(purchaseArchiveEntry2.getCountHowManyTimesHasBeenSold()).thenReturn(4);
        itemPurchaseArchiveEntryHashMap.put(1, purchaseArchiveEntry1);
        itemPurchaseArchiveEntryHashMap.put(2, purchaseArchiveEntry2);

        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchiveEntryHashMap, orderArchive);
        int result1 = purchasesArchive.getHowManyTimesHasBeenItemSold(item1);
        int result2 = purchasesArchive.getHowManyTimesHasBeenItemSold(item2);

        verify(purchaseArchiveEntry1, times(1)).getCountHowManyTimesHasBeenSold();
        verify(purchaseArchiveEntry2, times(1)).getCountHowManyTimesHasBeenSold();
        Assertions.assertEquals(2, result1);
        Assertions.assertEquals(4, result2);
    }


    @Test
    public void getHowManyTimesHasBeenItemSold_ItemHasNotBeenSoldYet_Zero() {
        when(purchaseArchiveEntry1.getCountHowManyTimesHasBeenSold()).thenReturn(2);
        when(purchaseArchiveEntry2.getCountHowManyTimesHasBeenSold()).thenReturn(4);
        itemPurchaseArchiveEntryHashMap.put(1, purchaseArchiveEntry1);
        itemPurchaseArchiveEntryHashMap.put(2, purchaseArchiveEntry2);
        Item item = mock(Item.class);
        when(item.getID()).thenReturn(3);

        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchiveEntryHashMap, orderArchive);
        int result = purchasesArchive.getHowManyTimesHasBeenItemSold(item);

        verify(purchaseArchiveEntry1, times(0)).getCountHowManyTimesHasBeenSold();
        verify(purchaseArchiveEntry2, times(0)).getCountHowManyTimesHasBeenSold();
        Assertions.assertEquals(0, result);
    }


    @Test
    public void putOrderToPurchasesArchive_TestingPutOfAlreadyExistingItems_UnchangedSizeOfHashMap() {
        ArrayList itemList = new ArrayList();
        itemList.add(item1);
        itemList.add(item2);
        ShoppingCart shoppingCart = new ShoppingCart(itemList);
        Order order = new Order(shoppingCart, "Mirza", "Srbsko");
        itemPurchaseArchiveEntryHashMap.put(1, purchaseArchiveEntry1);
        itemPurchaseArchiveEntryHashMap.put(2, purchaseArchiveEntry2);

        int size = itemPurchaseArchiveEntryHashMap.size();
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchiveEntryHashMap, orderArchive);
        purchasesArchive.putOrderToPurchasesArchive(order);

        verify(orderArchive, times(1)).add(order);
        Assertions.assertEquals(size, purchasesArchive.getItemPurchaseArchive().size());
    }


    @Test
    public void putOrderToPurchasesArchive_TestingPutOfANewItemAndAKnownItem_OneBiggerSizeOfHashMapAndCount() {
        ArrayList itemList = new ArrayList();
        itemList.add(item1);
        itemList.add(item2);
        ShoppingCart sc = new ShoppingCart(itemList);
        Order order = new Order(sc, "Kamilka", "Brnky");
        ItemPurchaseArchiveEntry purchaseArchiveEntry1 = new ItemPurchaseArchiveEntry(item1);
        itemPurchaseArchiveEntryHashMap.put(1, purchaseArchiveEntry1);
        
        int size = itemPurchaseArchiveEntryHashMap.size();
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchiveEntryHashMap, orderArchive);
        purchasesArchive.putOrderToPurchasesArchive(order);
        
        verify(orderArchive, times(1)).add(order);
        Assertions.assertEquals(size + 1, purchasesArchive.getItemPurchaseArchive().size());
        Assertions.assertEquals(2, purchasesArchive.getItemPurchaseArchive().get(1).getCountHowManyTimesHasBeenSold());
    }


    @Test
    public void purchasesArchive_ControllingConstructor_EqualItemsId(){
        itemPurchaseArchiveEntryHashMap.put(1, new ItemPurchaseArchiveEntry(item1));
        itemPurchaseArchiveEntryHashMap.put(2, new ItemPurchaseArchiveEntry(item2));

        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchiveEntryHashMap, orderArchive);

        Assertions.assertEquals(1, purchasesArchive.getItemPurchaseArchive().get(1).getRefItem().getID());
        Assertions.assertEquals(2, purchasesArchive.getItemPurchaseArchive().get(2).getRefItem().getID());
    }
}
