package storage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Item;

import static org.junit.jupiter.api.Assertions.*;

public class ItemStockTest {
    Item item = new Item(0,"football", 69.88f, "football"){};
    ItemStock itemStock;

    @BeforeEach
    public void initializeItemStock() {
        itemStock = new ItemStock(item);
    }

    @Test
    public void itemStockConstuctor_TestingConstuctor_Object() {
        int expectedCount = 0;

        Assertions.assertEquals(item, itemStock.getItem());
        Assertions.assertEquals(expectedCount, itemStock.getCount());
    }

    @ParameterizedTest(name = "0 plus {0} should return {1}")
    @CsvSource({"1, 1", "-1, -1"})
    void increaseItemCount_AddValueToZero_OneThenMinusOne(int increment, int result) {
        itemStock.IncreaseItemCount(increment);
        Assertions.assertEquals(result, itemStock.getCount());
    }

    @ParameterizedTest(name = "0 minus {0} should return {1}")
    @CsvSource({"1, -1", "-1, 1"})
    void decreaseItemCount_DeductValueFromZero_MinusOneThenOne(int decrement, int result) {
        itemStock.decreaseItemCount(decrement);
        Assertions.assertEquals(result, itemStock.getCount());
    }
}