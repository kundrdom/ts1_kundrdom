package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import storage.NoItemInStorage;


public class EShopControllerTest {
    EShopController eShopController;
    Item[] storageItems = {
            new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
            new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
            new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
            new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
            new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
            new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
    };

    @BeforeEach
    public void setup() {
        eShopController = new EShopController();
        int[] itemCount = {10,10,4,5,10,2};

        for (int i = 0; i < storageItems.length; i++) {
            eShopController.getStorage().insertItems(storageItems[i], itemCount[i]);
        }
    }

    @Test
    public void eShopController_ProcessTestForNormalShopping1_EverythingAccordingToPlan() throws Exception {
        Assertions.assertEquals(10, eShopController.getStorage().getItemCount(1));
        ShoppingCart newCart = new ShoppingCart();
        newCart.addItem(storageItems[0]);
        newCart.addItem(storageItems[1]);
        newCart.addItem(storageItems[2]);
        newCart.addItem(storageItems[5]);

        Assertions.assertEquals(4, newCart.getItemsCount());
        Assertions.assertEquals(11920, newCart.getTotalPrice());

        newCart.removeItem(1);
        Assertions.assertEquals(3, newCart.getItemsCount());

        Assertions.assertThrows(Exception.class, () -> {newCart.removeItem(1);});

        Assertions.assertEquals(6920, newCart.getTotalPrice());

        EShopController.purchaseShoppingCart(newCart, "Kamilka", "Zimbabwe");
        Assertions.assertEquals(10, eShopController.getStorage().getItemCount(1));
        Assertions.assertEquals(9, eShopController.getStorage().getItemCount(2));
        Assertions.assertEquals(3, eShopController.getStorage().getItemCount(3));
        Assertions.assertEquals(5, eShopController.getStorage().getItemCount(4));
        Assertions.assertEquals(10, eShopController.getStorage().getItemCount(5));
        Assertions.assertEquals(1, eShopController.getStorage().getItemCount(6));

        Assertions.assertEquals(3, eShopController.getArchive().getItemPurchaseArchive().size());
        Assertions.assertEquals(1, eShopController.getArchive().getItemPurchaseArchive().get(2).getCountHowManyTimesHasBeenSold());
        Assertions.assertEquals(1, eShopController.getArchive().getItemPurchaseArchive().get(3).getCountHowManyTimesHasBeenSold());
        Assertions.assertEquals(1, eShopController.getArchive().getItemPurchaseArchive().get(6).getCountHowManyTimesHasBeenSold());
    }

    @Test
    public void eShopController_ProcessTestForNormalShopping3_EverythingGoesAccordingToPlan() throws Exception {
        ShoppingCart newCart = new ShoppingCart();
        newCart.addItem(storageItems[5]);
        newCart.addItem(storageItems[5]);
        eShopController.purchaseShoppingCart(newCart, "Vojta", "Cimice");
        Assertions.assertEquals(0, eShopController.getStorage().getItemCount(6));
        Assertions.assertEquals(2, eShopController.getArchive().getItemPurchaseArchive().get(6).getCountHowManyTimesHasBeenSold());

        Assertions.assertThrows(NoItemInStorage.class, () ->
        {eShopController.purchaseShoppingCart(newCart, "Jirka", "Nebelvir");});
    }
}
