package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class OrderTest {
    ArrayList<Item> items = new ArrayList<>();
    Item item;
    Order order;
    ShoppingCart cart;

    @BeforeEach
    public void setUpOrder() {
        item = new Item(0,"football", 69.88f, "football"){};
        cart = new ShoppingCart(items);
    }

    @Test
    public void orderConstructor_TestingConstructor1_Object() {
        items.add(item);
        order = new Order(cart, "Jirka Varecka", "Lososova", 88);

        String expectedCustomerName = "Jirka Varecka";
        String expectedCustomerAddress = "Lososova";
        int expectedState = 88;

        Assertions.assertEquals(item, order.getItems().get(0));
        Assertions.assertEquals(expectedCustomerName, order.getCustomerName());
        Assertions.assertEquals(expectedCustomerAddress, order.getCustomerAddress());
        Assertions.assertEquals(expectedState, order.getState());
    }

    @Test
    public void orderConstructor_TestingConstructor2_Object() {
        items.add(item);
        Order order = new Order(cart, "Jirka Varecka", "Lososova");

        String expectedCustomerName = "Jirka Varecka";
        String expectedCustomerAddress = "Lososova";
        int expectedState = 0;

        Assertions.assertEquals(item, order.getItems().get(0));
        Assertions.assertEquals(expectedCustomerName, order.getCustomerName());
        Assertions.assertEquals(expectedCustomerAddress, order.getCustomerAddress());
        Assertions.assertEquals(expectedState, order.getState());
    }

    @Test
    public void getItems_TestingItemsIfNull_True() {
        cart = new ShoppingCart(null);
        Order order = new Order(cart, "", "");

        String expectedCustomerName = "";
        String expectedCustomerAddress = "";
        int expectedState = 0;

        Assertions.assertNull(order.getItems());
        Assertions.assertEquals(expectedCustomerName, order.getCustomerName());
        Assertions.assertEquals(expectedCustomerAddress, order.getCustomerAddress());
        Assertions.assertEquals(expectedState, order.getState());
    }

    @Test
    public void getItems_TestingItemsIfNull2_True() {
        cart = new ShoppingCart(null);
        Order order = new Order(cart, "", "",88);

        String expectedCustomerName = "";
        String expectedCustomerAddress = "";
        int expectedState = 88;

        Assertions.assertNull(order.getItems());
        Assertions.assertEquals(expectedCustomerName, order.getCustomerName());
        Assertions.assertEquals(expectedCustomerAddress, order.getCustomerAddress());
        Assertions.assertEquals(expectedState, order.getState());
    }
}
