package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class StandardItemTest {
    StandardItem standardItem;
    @BeforeEach
    public void createItem() {
        standardItem = new StandardItem(0,"football", 69.88F
                , "football", 88);
    }

    @Test
    public void constructorTest_TestingConstructor_Object() {
        int expectedId = 0;
        String expectedName = "football";
        float expectedPrice = 69.88F;
        String expectedCategory = "football";
        int expectedLoyaltyPoints = 88;

        Assertions.assertEquals(expectedId, standardItem.getID());
        Assertions.assertEquals(expectedName, standardItem.getName());
        Assertions.assertEquals(expectedPrice, standardItem.getPrice());
        Assertions.assertEquals(expectedCategory, standardItem.getCategory());
        Assertions.assertEquals(expectedLoyaltyPoints, standardItem.getLoyaltyPoints());
    }

    @Test
    public void copyTest_ComparingTwoObjects_Object() {
        StandardItem mirza = standardItem.copy();

        Assertions.assertEquals(standardItem, mirza);
    }

    @ParameterizedTest
    @CsvSource({"1, football, 69.88, football, 88, 1, football, 69.88, football, 88",
    "7, kamilka, 88, cerni pracovnici, 9, 7, kamilka, 88, cerni pracovnici, 9",
    "9, mirza, 1, srbove, 0, 9, mirza, 1, srbove, 0"})
    public void equals_ParametrizedTest_True(int id, String name, float price, String category,
                                             int loyaltyPoints, int id2, String name2, float price2,
                                             String category2, int loyaltyPoints2) {
        StandardItem okurka = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem moucnik = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);

        boolean result = okurka.equals(moucnik);

        Assertions.assertTrue(result);
    }
}
