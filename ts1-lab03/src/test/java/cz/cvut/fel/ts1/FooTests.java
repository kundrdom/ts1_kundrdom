package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class FooTests {

    // NázevMetody_TestovanýStav_OčekávanýVýstup

    Foo foo;

    @BeforeEach
    public void SingleTestSetUp() {
        foo = new Foo();
    }

    @Test
    public void ReturnNumber_ReturningSpecificNumber_Five() {
        // ARRANGE
        int expectedValue = 5;
        // ACT
        int result = foo.returnNumber();
        // ASSERT
        Assertions.assertEquals(expectedValue,result);
    }

    @Test
    public void GetNum_GetDefaultNumber_Zero() {
        // ARRANGE
        int expectedValue = 0;
        // ACT
        int result = foo.getNum();
        // ASSERT
        Assertions.assertEquals(expectedValue,result);
    }

    @Test
    public void Increment_IncrementingValueByOne_One() {
        // ARRANGE
        int expectedValue = foo.getNum() + 1;
        // ACT
        foo.increment();
        // ASSERT
        Assertions.assertEquals(expectedValue,foo.getNum());
    }

    @Test
    public void ExceptionThrown_CallingMethod_Exception() throws Exception {
        // ARRANGE
        // ACT + ASSERT
        Assertions.assertThrows(Exception.class, () -> {
           foo.exceptionThrown();
        });
    }

}
