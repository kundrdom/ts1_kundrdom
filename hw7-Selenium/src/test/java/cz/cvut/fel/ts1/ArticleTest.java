package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ArticleTest {
    private WebDriver driver;

    @BeforeEach
    public void setUp(){
        this.driver = new ChromeDriver();
    }

    @Test
    public void TestingGoodNamesFromArticle_GetsNamesFromArticle_NamesEquals() {
        driver.get("https://link.springer.com/article/10.1007/s41095-022-0271-y");
        Article article = new Article(driver);
        Assertions.assertEquals("Attention mechanisms in computer vision: A survey", article.getTitle());
        Assertions.assertEquals("15 March 2022", article.getTime());
        Assertions.assertEquals("https://doi.org/10.1007/s41095-022-0271-y", article.getDOI());
    }
}

