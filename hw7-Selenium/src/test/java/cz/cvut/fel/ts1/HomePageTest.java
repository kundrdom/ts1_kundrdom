package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomePageTest {
    private WebDriver driver;

    @BeforeEach
    public void setUp(){
        this.driver = new ChromeDriver();
    }

    @Test
    public void searchBarAndButtonTest_SearchesRightWord_RedirectsToRightPage() {
        HomePage homePage = new HomePage(driver);
        homePage.insertWord("kamilka");
        homePage.search();
        Assertions.assertEquals( "https://link.springer.com/search?query=kamilka", this.driver.getCurrentUrl());
    }

    @Test
    public void testForSignUp_RedirectsToSignUpPage_EverythingGoesWell() {
        HomePage homePage = new HomePage(driver);
        driver.manage().window().fullscreen();
        homePage.signUp();
        Assertions.assertEquals("https://link.springer.com/signup-login?previousUrl=https%3A%2F%2Flink.springer.com%2F", this.driver.getCurrentUrl());
    }
}
