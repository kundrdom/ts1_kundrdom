package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AdvancedSearchTest {
    private WebDriver driver;

    @BeforeEach
    public void setUp(){
        this.driver = new ChromeDriver();
    }

    @Test
    public void testForAdvancedSearch_SearchesAnArticle_AllGoesAccordingToPlan() {
        AdvancedSearch advancedSearch = new AdvancedSearch(this.driver);
        advancedSearch.setEverything("misa", "is", "misa", "smart", "misa", "Kae Ishii");
        advancedSearch.setTime("2000", "2020");
        advancedSearch.search();
        Assertions.assertEquals("https://link.springer.com/search?date-facet-mode=between&facet-start-year=2000&showAll=true&facet-end-year=2020&dc.title=misa&query=misa+AND+%22is%22+AND+%28misa%29+AND+NOT+%28smart%29&dc.creator=Kae+Ishii",
                driver.getCurrentUrl());
    }
}

