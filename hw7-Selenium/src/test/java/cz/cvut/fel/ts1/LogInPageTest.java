package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LogInPageTest {
    private WebDriver driver;

    @BeforeEach
    public void setUp(){
        driver = new ChromeDriver();
    }

    @Test
    public void testForLogIn_SendsToHomePage_GetsLoggedIn() {
        LogInPage logInPage = new LogInPage(driver);
        logInPage.setEmailInput("mirzajedebil@gmail.com");
        logInPage.setPasswordInput("Mirzajekokot88");
        logInPage.logIn();
        Assertions.assertEquals("https://link.springer.com/",driver.getCurrentUrl());
    }

}
