package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SeleniumTestingTest {
    private WebDriver driver;
    private final FileHelper fileHelper = new FileHelper();

    private Search search;
    private HomePage homePage;
    private Article article;
    private LogInPage logInPage;


    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();
    }

    @Test
    @Order(1)
    public void testWritingIntoFile_WriteIntoFile_EverythingGoesWell() {
        int articleNumToLoad = 4;
        List<String> articleInfo = new ArrayList<>();

        // set up
        AdvancedSearch advancedSearch = new AdvancedSearch(driver);
        search = new Search(driver);
        homePage = new HomePage(driver);
        article = new Article(driver);
        logInPage = new LogInPage(driver);

        // logs in
        driver.get("https://link.springer.com/signup-login");
        logInPage.setEmailInput("mirzajedebil@gmail.com");
        logInPage.setPasswordInput("Mirzajekokot88");
        logInPage.logIn();

        // advance search
        homePage.goToAdvancedSearch();
        advancedSearch.setEverything("Page Object Model", "", "Sellenium Testing", "", "", "");
        advancedSearch.setTime("2022");
        advancedSearch.search();

        search.filterOnlyArticles();

        for (int articleNumber = 1; articleNumber <= articleNumToLoad; articleNumber++) {
            search.openArticle(articleNumber);

            String articleName = article.getTitle();
            String articleDate = article.getTime();
            String articleDOI = article.getDOI();

            articleInfo.add(articleName + "," + articleDate + "," + articleDOI);

            driver.navigate().back();
        }

        // write into file
        try {
            //File for parametrized tests
            String articleInfoFileName = "src/main/resources/articleData.csv";
            fileHelper.writeIntoFile(articleInfo, articleInfoFileName);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @ParameterizedTest(name = "Article named {0} published {1} with DOI {2}")
    @Order(2)
    @CsvFileSource(resources = "/articleData.csv")
    public void testFile_TestIfValuesFromFileAreEqual_EverythingEquals(String name, String datePublished, String DOI) {
        // set up
        search = new Search(driver);
        homePage = new HomePage(driver);
        article = new Article(driver);
        logInPage = new LogInPage(driver);

        // logs in
        driver.get("https://link.springer.com/signup-login");
        logInPage.setEmailInput("mirzajedebil@gmail.com");
        logInPage.setPasswordInput("Mirzajekokot88");
        logInPage.logIn();

        // search article
        homePage.insertWord(name);
        homePage.search();

        // open first article
        search.openArticle(1);

        Assertions.assertEquals(name, article.getTitle());
        Assertions.assertEquals(datePublished, article.getTime());
        Assertions.assertEquals(DOI, article.getDOI());
    }
}

