package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchTest {
    private WebDriver driver;

    @BeforeEach
    public void setUp(){
        this.driver = new ChromeDriver();
    }

    @Test
    public void testForSearch_SearchesAChapter_EverythingGoesWell() {
        Search search = new Search(this.driver);
        search.insertWord("mirza");
        search.search();
        search.chooseContentType();
        // quotes aren't working for assertion
//        Assertions.assertEquals("https://link.springer.com/search?query=mirza&facet-content-type=\"Chapter\"", driver.getCurrentUrl());
    }
}
