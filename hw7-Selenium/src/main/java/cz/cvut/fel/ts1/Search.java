package cz.cvut.fel.ts1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Search {
    private final WebDriver driver;
    @FindBy(id = "query")
    private WebElement searchBar;
    @FindBy(id = "search")
    private WebElement searchButton;
    @FindBy(xpath = "//*[text()='Article']")
    private WebElement articleFilterButtton;



    public Search(WebDriver driver) {
        this.driver = driver;
        driver.get("https://link.springer.com/search");
        PageFactory.initElements(driver, this);
    }

    public void insertWord(String searchWord) {
        this.searchBar.sendKeys(searchWord);
    }

    public void search() {
        this.searchButton.click();
    }

    public void chooseContentType() {
        driver.get(driver.getCurrentUrl() + "&facet-content-type=\"Chapter\"");
    }

    public void filterOnlyArticles(){
        articleFilterButtton.click();
    }

    public void openArticle(int number) {
        String link = "";
        link = this.driver.findElement(By.xpath("//ol[@id='results-list']/li[" + number + "]/h2/a")).getAttribute("href");
        this.driver.get(link);
    }

    public WebDriver getDriver() {
        return driver;
    }
}
