package cz.cvut.fel.ts1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogInPage {
    private final WebDriver driver;
    @FindBy(id = "login-box-email")
    private WebElement emailInput;
    @FindBy(id = "login-box-pw")
    private WebElement passwordInput;


    public LogInPage(WebDriver driver) {
        this.driver = driver;
        driver.get("https://link.springer.com/signup-login");
        PageFactory.initElements(driver, this);
    }

    public void setEmailInput(String emailInput) {
        this.emailInput.sendKeys(emailInput);
    }

    public void setPasswordInput(String passwordInput) {
        this.passwordInput.sendKeys(passwordInput);
    }

    public void logIn() {
        this.driver.findElement(By.xpath("//div[@class='form-submit']/button")).click();
    }

}
