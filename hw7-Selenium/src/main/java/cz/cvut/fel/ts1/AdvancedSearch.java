package cz.cvut.fel.ts1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AdvancedSearch {
    private final WebDriver driver;
    @FindBy(id = "all-words")
    private WebElement wordsInput;
    @FindBy(id = "exact-phrase")
    private WebElement exactPhrase;
    @FindBy(id = "least-words")
    private WebElement oneOfTheWords;
    @FindBy(id = "without-words")
    private WebElement withoutWords;
    @FindBy(id = "title-is")
    private WebElement title;
    @FindBy(id = "author-is")
    private WebElement author;
    @FindBy(id = "date-facet-mode")
    private WebElement inBetween;
    @FindBy(id = "facet-start-year")
    private WebElement startYear;
    @FindBy(id = "facet-end-year")
    private WebElement endYear;
    @FindBy(id = "submit-advanced-search")
    private WebElement searchButton;


    public AdvancedSearch(WebDriver driver) {
        this.driver = driver;
        driver.get("https://link.springer.com/advanced-search");
        PageFactory.initElements(driver, this);
    }

    public void setEverything(String wordsInput, String exactPhrase, String oneOfTheWords, String withoutWords, String titleContains, String author) {
        this.wordsInput.sendKeys(wordsInput);
        this.exactPhrase.sendKeys(exactPhrase);
        this.oneOfTheWords.sendKeys(oneOfTheWords);
        this.withoutWords.sendKeys(withoutWords);
        this.title.sendKeys(titleContains);
        this.author.sendKeys(author);
    }

    public void setTime(String startYear, String endYear) {
        Select selectBetween = new Select(inBetween);
        selectBetween.selectByVisibleText("between");
        this.startYear.sendKeys(startYear);
        this.endYear.sendKeys(endYear);
    }

    public void setTime(String year) {
        Select selectIn = new Select(inBetween);
        selectIn.selectByVisibleText("in");
        this.startYear.sendKeys(year);

    }

    public void search() {
        searchButton.click();
    }

    public WebDriver getDriver() {
        return driver;
    }
}
