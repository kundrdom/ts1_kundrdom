package cz.cvut.fel.ts1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    private final WebDriver driver;
    @FindBy(id = "query")
    private WebElement searchBar;
    @FindBy(id = "search")
    private WebElement searchButton;
    @FindBy(id = "search-options")
    private WebElement advancedSearchOption;


    public HomePage(WebDriver driver) {
        this.driver = driver;
        driver.get("https://link.springer.com");
        PageFactory.initElements(driver, this);
    }

    public void signUp() {
        //this.driver.findElement(By.className("register-link flyout-caption")).click();
        driver.findElement(By.xpath("//a[@class='register-link flyout-caption']")).click();
    }

    public void insertWord(String searchWord) {
        searchBar.sendKeys(searchWord);
    }

    public void search() {
        searchButton.click();
    }

    public void goToAdvancedSearch(){
        advancedSearchOption.click();
        driver.findElement(By.id("advanced-search-link")).click();
    }
}

