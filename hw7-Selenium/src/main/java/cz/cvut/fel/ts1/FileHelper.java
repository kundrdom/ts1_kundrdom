package cz.cvut.fel.ts1;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileHelper {

    public void writeIntoFile(List<String> data, String fileName) throws IOException {
        FileWriter fw = new FileWriter(fileName);
        for (String info : data) {
            fw.write(info + "\n");
        }
        fw.close();
    }
}
