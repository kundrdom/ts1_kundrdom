package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    Calculator calculator = new Calculator();
    int a = 0;
    int b = 0;

    @Test
    public void Add_AddingTwoNumbers_Three() {
        // ARRANGE
        a = 1;
        b = 2;
        int expectedValue = 3;
        // ACT
        int result = calculator.add(a,b);
        // ASSERT
        Assertions.assertEquals(expectedValue,result);
    }

    @Test
    public void Subtract_SubtractTwoNumbers_Three() {
        // ARRANGE
        a = 5;
        b = 2;
        int expectedValue = 3;
        // ACT
        int result = calculator.subtract(a,b);
        // ASSERT
        Assertions.assertEquals(expectedValue,result);
    }

    @Test
    public void Multiply_MultiplyingTwoNumbers_Three() {
        // ARRANGE
        a = 1;
        b = 3;
        int expectedValue = 3;
        // ACT
        int result = calculator.multiply(a,b);
        // ASSERT
        Assertions.assertEquals(expectedValue,result);
    }

    @Test
    public void Divide_DividingTwoNumbers_Three() {
        // ARRANGE
        a = 3;
        b = 1;
        int expectedValue = 3;
        // ACT
        int result = calculator.divide(a,b);
        // ASSERT
        Assertions.assertEquals(expectedValue,result);
    }

    @Test
    public void Divide_DividingByZero_Exception()  {
        // ARRANGE
        a = 3;
        b = 0;
        // ACT + ASSERT
        Assertions.assertThrows(ArithmeticException.class, () -> calculator.divide(a,b));
    }

}
